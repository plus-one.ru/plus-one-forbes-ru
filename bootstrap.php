<?php

require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$parseURL = parse_url($_SERVER['REQUEST_URI']);

if ($parseURL['path'] === '/217471-8' || $parseURL['path'] === '/217471-8.html') {
		$blogClass->body = file_get_contents('./design/forbes/html/217471-8.tpl');
}
else {
		$fetch = $blogClass->fetch();
		if ($fetch === false){
			echo  file_get_contents('custom-404.html');
			exit(0);
		}
}

$ogMeta = false;

$url = explode("/", $parseURL['path']);

$rootUrl = $Widget->root_url_for_link;

$meta = array(
    'title' => 'Forbes +1',
    'description' => '+1 — коммуникационный проект, рассказывающий о лидерских практиках в области социальной и экологической ответственности. Миссия Проекта +1 — содействовать развитию инфраструктуры рынка устойчивого развития. Наши партнеры создают социально значимые проекты, мы рассказываем о них массовой аудитории и формируем культуру ответственного производства, потребления и инвестирования.',
    'keywords' => 'Forbes +1'
);



if (!empty($url[1])){
    $result = $blogClass->getOgTags(str_replace("?", "", $url[1]));
    $resultArray = $blogClass->getOgTags(str_replace("?", "", $url[1]), true);
    $meta['title'] = $resultArray['title'];
    $meta['description'] = $resultArray['description'];
    $meta['keywords'] = $resultArray['keywords'];
    $ogMeta = true;
}
