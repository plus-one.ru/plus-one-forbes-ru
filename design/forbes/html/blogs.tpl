<div class="materials-on-tag__wrap">
    <h2 class="materials-on-tag__title">
        <span class="materials-on-tag__title-item">Материалы по тегу</span> <br>
        <span class="materials-on-tag__title-item">#{$header}</span>
    </h2>
    <div class="materials-on-tag__content">
        {foreach item=post from=$items name=post}
            <a href="/{$post->url}">
                <div class="materials-on-tag__item">
                    {$post->name}
                </div>
                <div class="materials-line"></div>
            </a>
        {/foreach}
    </div>
</div>
