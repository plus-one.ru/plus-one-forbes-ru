<table class="table">
    {foreach from=$posts_short_list item=postItem name=postItem}
    <tr>
        <td style="width: 10%;">
            <img src="/files/blogposts/{$postItem->id}-1_2.jpg" style="width: 80px;" />
        </td>
        <td style="width: 80%;">
            {$postItem->name|escape}
        </td>
        <td style="width: 10%;">
            <div class="btn-group">
                <a href="#" type="button" post_id="{$postItem->id}" onClick="return false;" class="btn btn-success btn-xs link_to_linked_post">
                    <i class="fa fa-link"></i> Связать
                </a>
            </div>
        </td>
    </tr>
    {/foreach}
</table>

