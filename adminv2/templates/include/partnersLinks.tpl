<div class="well">
    <div class="row">

        {* ПАРТНЕР 1 *}
        <div class="col-lg-4">
            <div class="well">
                <h4>Изображение</h4>

                <div class="row">
                    <div class="col-lg-12">
                        {if $partnersPosts[0]->image}
                            <img id="image_partnerlinks1" class="image_preview"
                                 src='{$images_uploaddir}{$partnersPosts[0]->image}'
                                 alt="" style="width: 100%;  height: 100%"/>
                        {else}
                            <img id="image_partnerlinks1" class="image_preview"
                                 src='images/no_foto.gif' alt=""/>
                        {/if}
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Выберите файл для загрузки</label>
                            <input name="partnerimage1" type="file"
                                   style="width: 100%; overflow: hidden;"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Заголовок</label>
                    <textarea rows="5" name="partnerlink[1][header]"
                              class="form-control">{$partnersPosts[0]->header}</textarea>
                </div>

                <div class="form-group">
                    <label>Подзаголовок</label>
                    <textarea rows="5" name="partnerlink[1][subheader]"
                              class="form-control">{$partnersPosts[0]->subheader}</textarea>
                </div>

                <div class="form-group">
                    <label>Анонс</label>
                    <select class="form-control" name="partnerlink[1][rubrika_id]">
                        <option value="0">Ни одного</option>
                        {foreach item=tag from=$tags name=tag}
                            {if $tag->url != 'main'}
                                <option value="{$tag->id}"
                                        {if $partnersPosts[0]->rubrika_id==$tag->id}selected{/if}>{$tag->name|escape}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>

                <div class="form-group">
                    <label>Партнер записи</label>
                    <select class="form-control" name="partnerlink[1][partner_id]">
                        <option value="0"> -- Выберите партнера --</option>
                        {foreach item=partner from=$partners name=partner}
                            <option value="{$partner->id}"
                                    {if $partnersPosts[0]->partner_id==$partner->id}selected{/if}>{$partner->name}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="form-group">
                    <label>Ссылка на статью партнера</label>
                    <input name="partnerlink[1][link]" type="text" value='{$partnersPosts[0]->link}' class="form-control">
                </div>
            </div>
        </div>

        {* ПЕРТНЕР 2*}
        <div class="col-lg-4">
            <div class="well">
                <h4>Изображение</h4>

                <div class="row">
                    <div class="col-lg-12">
                        {if $partnersPosts[1]->image}
                            <img id="image_partnerlinks2" class="image_preview"
                                 src='{$images_uploaddir}{$partnersPosts[1]->image}'
                                 alt="" style="width: 100%;  height: 100%"/>
                        {else}
                            <img id="image_partnerlinks2" class="image_preview"
                                 src='images/no_foto.gif' alt=""/>
                        {/if}
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Выберите файл для загрузки</label>
                            <input name="partnerimage2" type="file"
                                   style="width: 100%; overflow: hidden;"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Заголовок</label>
                    <textarea rows="5" name="partnerlink[2][header]"
                              class="form-control">{$partnersPosts[1]->header}</textarea>
                </div>

                <div class="form-group">
                    <label>Подзаголовок</label>
                    <textarea rows="5" name="partnerlink[2][subheader]"
                              class="form-control">{$partnersPosts[1]->subheader}</textarea>
                </div>

                <div class="form-group">
                    <label>Анонс</label>
                    <select class="form-control" name="partnerlink[2][rubrika_id]">
                        <option value="0">Ни одного</option>
                        {foreach item=tag from=$tags name=tag}
                            {if $tag->url != 'main'}
                                <option value="{$tag->id}"
                                        {if $partnersPosts[1]->rubrika_id==$tag->id}selected{/if}>{$tag->name|escape}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>

                <div class="form-group">
                    <label>Партнер записи</label>
                    <select class="form-control" name="partnerlink[2][partner_id]">
                        <option value="0"> -- Выберите партнера --</option>
                        {foreach item=partner from=$partners name=partner}
                            <option value="{$partner->id}"
                                    {if $partnersPosts[1]->partner_id==$partner->id}selected{/if}>{$partner->name}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="form-group">
                    <label>Ссылка на статью партнера</label>
                    <input name="partnerlink[2][link]" type="text" value='{$partnersPosts[1]->link}' class="form-control">
                </div>
            </div>
        </div>

        {* ПАРТНЕР 3*}
        <div class="col-lg-4">
            <div class="well">
                <h4>Изображение</h4>

                <div class="row">
                    <div class="col-lg-12">
                        {if $partnersPosts[2]->image}
                            <img id="image_partnerlinks3" class="image_preview"
                                 src='{$images_uploaddir}{$partnersPosts[2]->image}'
                                 alt="" style="width: 100%;  height: 100%"/>
                        {else}
                            <img id="image_partnerlinks3" class="image_preview"
                                 src='images/no_foto.gif' alt=""/>
                        {/if}
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Выберите файл для загрузки</label>
                            <input name="partnerimage3" type="file"
                                   style="width: 100%; overflow: hidden;"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Заголовок</label>
                    <textarea rows="5" name="partnerlink[3][header]"
                              class="form-control">{$partnersPosts[2]->header}</textarea>
                </div>

                <div class="form-group">
                    <label>Подзаголовок</label>
                    <textarea rows="5" name="partnerlink[3][subheader]"
                              class="form-control">{$partnersPosts[2]->subheader}</textarea>
                </div>

                <div class="form-group">
                    <label>Анонс</label>
                    <select class="form-control" name="partnerlink[3][rubrika_id]">
                        <option value="0">Ни одного</option>
                        {foreach item=tag from=$tags name=tag}
                            {if $tag->url != 'main'}
                                <option value="{$tag->id}"
                                        {if $partnersPosts[2]->rubrika_id==$tag->id}selected{/if}>{$tag->name|escape}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>

                <div class="form-group">
                    <label>Партнер записи</label>
                    <select class="form-control" name="partnerlink[3][partner_id]">
                        <option value="0"> -- Выберите партнера --</option>
                        {foreach item=partner from=$partners name=partner}
                            <option value="{$partner->id}"
                                    {if $partnersPosts[2]->partner_id==$partner->id}selected{/if}>{$partner->name}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="form-group">
                    <label>Ссылка на статью партнера</label>
                    <input name="partnerlink[3][link]" type="text" value='{$partnersPosts[2]->link}' class="form-control">
                </div>
            </div>
        </div>
    </div>
</div>