<h5>Теги статьи <i class="glyphicon glyphicon-tags text-muted"></i> </h5>

<div class="form-group">
    <label {if !$Item->tags}class="text-danger"{/if}>Первого уровеня</label>
    <div class="form-group">
        {foreach item=tag from=$tags name=tag}
            {if $tag->url != 'main'}
                <label class="checkbox-inline">
                    <input type="radio" name="tags" value="{$tag->id}" data-str="{$tag->name|escape}" data-url="/{$tag->url|escape}"
                           {if $tag->id == $Item->tags}checked{/if}> {$tag->name|escape}
                </label>
            {/if}
        {/foreach}
    </div>
</div>
<div class="form-group">
    <label>Для внутренней страницы</label>
    <select class="selectpicker form-control" data-max-options="2" multiple data-live-search="true" name="post_tags[]" title="-- Укажите тег статьи для внутренней страницы --">
        {foreach item=tag from=$tags name=tag}
            {if $tag->items}
                <optgroup label="{$tag->name}">
                    {foreach item=tagItem from=$tag->items name=tagItem}
                        <option value="{$tagItem->id}" {if $tagItem->check == 1}selected{/if}  data-url="/{$tag->url|escape}/{$tagItem->url|escape}">{$tagItem->name|escape}</option>
                    {/foreach}
                </optgroup>
            {/if}
        {/foreach}
    </select>
</div>