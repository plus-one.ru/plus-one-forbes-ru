{if $linkedPosts}
<table class="table">
    {foreach from=$linkedPosts item=linkedPost name=linkedPost}
        <tr>
            <td style="width: 10%;">
                {*<img src="../files/products/{$product_short->small_image}" style="width: 80px;" />*}
                <img src="/files/blogposts/{$linkedPost->id}-1_2.jpg" style="width: 80px;" />
            </td>
            <td style="width: 80%;">
                {$linkedPost->name|escape}
            </td>
            <td style="width: 10%;">
                <div class="btn-group">
                    <a href="#" type="button" post_id="{$linkedPost->id}" onClick="return false;" class="btn btn-success btn-xs unlink_to_linked_post">
                        <i class="fa fa-link"></i> Отвязать
                    </a>
                </div>
            </td>
        </tr>
    {/foreach}
</table>
{else}
    <p>Нет связанных статей.</p>
    <p>Для привязки к текущей статье сопутствующих - выберите автора, а затем выберите необходимую статью из списка, и нажмите кнопку "Связать"</p>
{/if}