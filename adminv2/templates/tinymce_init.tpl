<script  type="text/javascript" src="js/tinymce/tinymce.js?v=44"></script>
<script>
	tinymce.init({literal}{{/literal}
		selector: '.smalleditor',
		height: 600,
		theme: 'modern',
        menubar: false,
            valid_children: '+a[div|p|h1|h2|h3|h4|h5|h6|span]',
            cleanup_on_startup: false,
            trim_span_elements: false,
            verify_html: false,
            convert_urls: false,
            cleanup: false,
        forced_root_block : 'p',
        {*forced_root_block_attrs: {literal}{{/literal}*}
            {*'class': 'text-block',*}
        {*{literal}}{/literal},*}
		plugins: [
		'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		'searchreplace visualblocks visualchars code fullscreen',
		'insertdatetime media nonbreaking save table contextmenu directionality',
		'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc bigblock opinion video digit threeBlock bigvideo',
        'templatetools'
		],
        toolbar1: 'link | template | image | bigblock | bigvideo | code',
        // toolbar2: ' | opinion | video | digit | threeBlock | code',
		image_advtab: false,
        image_dimensions: false,
        templatetools_settings: {
		    templateSelectors: ['.article__text','.article__authors', '.article__info', '.article__box', '.article__image', '.article-info', '.article__main-image'],
        },
		templates: [
                {literal}{{/literal}
                    "title": "Новости одна строка",
                    // "description": "Some desc 2",
                    "url": "templates/anounces/newsOneLine.html"
                {literal}}{/literal},
                {literal}{{/literal}
                "title": "Новости две строки",
                // "description": "Some desc 2",
                "url": "templates/anounces/newsTwoLine.html"
                {literal}}{/literal},
                {literal}{{/literal}
                "title": "Новости три строки",
                // "description": "Some desc 2",
                "url": "templates/anounces/newsThreeLine.html"
                {literal}}{/literal},

                {literal}{{/literal}
                "title": "Двойной блок",
                // "description": "Some desc 2",
                "url": "templates/anounces/twoItemBlock.html"
                {literal}}{/literal},

                {literal}{{/literal}
                "title": "Тройной блок",
                // "description": "Some desc 2",
                "url": "templates/anounces/threeItemBlock.html"
                {literal}}{/literal},
		],
		content_css: [
		    "//{$root_url}/css/main.css",
		    "//{$root_url}/adminv2/css/admin_to_main.css?v=10"
        ],
        file_browser_callback :
            function(field_name, url, type, win){literal}{{/literal}

                var filebrowser = "filebrowser.php";
                filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;
                tinymce.activeEditor.windowManager.open({literal}{{/literal}
                    title : "Выбор изображения",
                    width : 800,
                    height : 600,
                    url : filebrowser
                    {literal}}{/literal}, {literal}{{/literal}
                    window : win,
                    input : field_name
                    {literal}}{/literal});
                    return false;
                {literal}}{/literal}
{literal}
	}
{/literal});


            tinymce.init({literal}{{/literal}
                        selector: '.fulleditor',
                        height: 500,
                        theme: 'modern',
                        menubar: false,
                        relative_urls: false,
                        valid_children: '+a[div|p|h1|h2|h3|h4|h5|h6|span]',
                        cleanup_on_startup: false,
                        trim_span_elements: false,
                        link_assume_external_targets: true,
                        verify_html: false,
                        convert_urls: false,
                        cleanup: false,
                        forced_root_block : 'p',
                    // visualblocks_default_state: true,
                    // end_container_on_empty_block: true,
                        {*forced_root_block : 'p',*}
                        {*forced_root_block_attrs: {literal}{{/literal}*}
                        {*'class': 'text-block',*}
                        {*{literal}}{/literal},*}
                        plugins: [
                            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                            'searchreplace visualblocks visualchars code fullscreen',
                            'insertdatetime media nonbreaking save table contextmenu directionality',
                            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc',
                            'bulletImage imageVert imageGallery signLine headerTwoLevel citateNew dialogBox',
                            'textColoredImg factInPost factImageInPost pictureInPost pictureLinkInPost authorSignInPost header2InPost',
                            'templatetools'
                        ],
                        // toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
                        // toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code',
                        toolbar1: 'link | template | image | code | fixedAllTemplate | freeAllTemplate',
                        // toolbar2: 'factInPost | factImageInPost | header2InPost | pictureInPost | pictureLinkInPost | authorSignInPost',
                        templatetools_settings: {
                            templateSelectors: ['.article__text','.article__authors', '.article__info', '.article__box', '.article__image', '.article-info', '.article__main-image'],
                        },
                        image_advtab: false,
                        image_dimensions: false,
                        templates: [
                                {literal}{{/literal}
                                "title": "Инфо блок",
                                // "description": "Some desc 2",
                                "url": "templates/post/articleInfo.html?v=4"
                                {literal}}{/literal},

                                {literal}{{/literal}
                                "title": "Большая картинка",
                                // "description": "Some desc 2",
                                "url": "templates/post/articleMainImage.html"
                                {literal}}{/literal},

                                {literal}{{/literal}
                                "title": "Основной блок с текстом статьи",
                                // "description": "Some desc 2",
                                "url": "templates/post/articleText.html"
                                {literal}}{/literal},

                                {literal}{{/literal}
                                "title": "Обычный параграф с текстом статьи",
                                // "description": "Some desc 2",
                                "url": "templates/post/articleSimpleText.html"
                                {literal}}{/literal},

                                {literal}{{/literal}
                                "title": "Цитата",
                                "description": "Цитата для вставки в основном блоке текста",
                                "url": "templates/post/articleCitate.html"
                                {literal}}{/literal},

                                {literal}{{/literal}
                                "title": "ASCII emoticon",
                                // "description": "Цитата для вставки в основном блоке текста",
                                "url": "templates/post/articleEmo.html"
                                {literal}}{/literal},

                                {literal}{{/literal}
                                "title": "Большой текстовый блок",
                                // "description": "Цитата для вставки в основном блоке текста",
                                "url": "templates/post/articleBigTextBlock.html"
                                {literal}}{/literal},

                                {literal}{{/literal}
                                "title": "Картинка с подписью",
                                // "description": "Цитата для вставки в основном блоке текста",
                                "url": "templates/post/articleImageWithCaption.html"
                                {literal}}{/literal},

                                {literal}{{/literal}
                                "title": "Подпись автора",
                                // "description": "Цитата для вставки в основном блоке текста",
                                "url": "templates/post/articleAuthorSign.html"
                                {literal}}{/literal},

                        ],
                        content_css: [
                            "//{$root_url}/css/main.css",
                            "//{$root_url}/adminv2/css/admin_to_main.css?v=10"
                        ],
                        init_instance_callback : "myCustomInitInstance",
                        file_browser_callback :
                                function(field_name, url, type, win){literal}{{/literal}
                                    var filebrowser = "filebrowser.php";
                                    filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;
                                    tinymce.activeEditor.windowManager.open({literal}{{/literal}
                                        title : "Выбор изображения",
                                        width : 800,
                                        height : 600,
                                        url : filebrowser
                                        {literal}}{/literal}, {literal}{{/literal}
                                        window : win,
                                        input : field_name
                                        {literal}}{/literal});
                                    return false;
                                    {literal}}{/literal}
                        {literal}
                    }
                    {/literal});

</script>
