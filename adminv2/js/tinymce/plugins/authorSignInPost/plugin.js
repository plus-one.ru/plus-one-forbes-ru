/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * digit plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('authorSignInPost', function(editor, url) {
	// Add a button that opens a window
	editor.addButton('authorSignInPost', {
		text: 'Подпись автора',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Подпись автора',
				body: [
                    {type: 'textbox', name: 'sign', label: 'Подпись'},
				],
				onsubmit: function(e) {
                    var content = '<div class="authors-block">\n' +
                        '<div class="divider"></div>\n' +
                        '<p>' + e.data.sign + '</p>\n' +
                        '</div>';
                    editor.insertContent(content);
				}
			});
		}
	});

    function srcChange(e) {
        var srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

        if (imageListCtrl) {
            imageListCtrl.value(editor.convertURL(this.value(), 'src'));
        }

        tinymce.each(meta, function(value, key) {
            win.find('#' + key).value(value);
        });

        if (!meta.width && !meta.height) {
            srcURL = editor.convertURL(this.value(), 'src');

            // Pattern test the src url and make sure we haven't already prepended the url
            prependURL = editor.settings.image_prepend_url;
            absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
            if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
                srcURL = prependURL + srcURL;
            }

            this.value(srcURL);
        }
    }

    function onBeforeCall(e) {
        e.meta = win.toJSON();
    }
});