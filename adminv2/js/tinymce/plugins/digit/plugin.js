/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * digit plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('digit', function(editor, url) {
	// Add a button that opens a window
	editor.addButton('digit', {
		text: 'Блок "Цифра"',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Блок "Цифра"',
				body: [
					{
					    label: 'Заголовок',
                        type: 'textbox',
                        name: 'title',
                        multiline: true,
                        minWidth: editor.getParam("code_dialog_width", 600),
                        minHeight: editor.getParam("code_dialog_height", Math.min(tinymce.DOM.getViewPort().h - 300, 200)),
                        spellcheck: false,
                        style: 'direction: ltr; text-align: left'
                    },
                    {type: 'textbox', name: 'subtitle', label: 'Подпись'},
                    {
                        name: 'src',
                        type: 'filepicker',
                        filetype: 'image',
                        label: 'Фото подложка',
                        onchange: srcChange,
                        onbeforecall: onBeforeCall
                    }
				],
				onsubmit: function(e) {

					var content = '<section class="main-article main-article--single main-article--num main-article--white main-article--white-color">\n' +
                        '<div class="main-article__item main-article__item--single main-article__item--num">\n' +
                        '<div class="main-article__num-title">цифра</div>\n' +
                        '<h1 class="main-article__title-2 main-article__title-5">' + e.data.title + '</h1>\n' +
                        '<div class="main-article__num-title">' + e.data.subtitle + '</div>\n' +
                        '</div>\n' +
                        '<div class="bg-stretch" style="background-image: url(' + e.data.src + ')"></div>\n' +
                        '</section>';

                    editor.insertContent(content);
				}
			});
		}
	});

    function srcChange(e) {
        var srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

        if (imageListCtrl) {
            imageListCtrl.value(editor.convertURL(this.value(), 'src'));
        }

        tinymce.each(meta, function(value, key) {
            win.find('#' + key).value(value);
        });

        if (!meta.width && !meta.height) {
            srcURL = editor.convertURL(this.value(), 'src');

            // Pattern test the src url and make sure we haven't already prepended the url
            prependURL = editor.settings.image_prepend_url;
            absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
            if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
                srcURL = prependURL + srcURL;
            }

            this.value(srcURL);
        }
    }

    function onBeforeCall(e) {
        e.meta = win.toJSON();
    }

});