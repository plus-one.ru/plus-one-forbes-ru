/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * bigblock plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('bigblock', function(editor, url) {
    // Add a button that opens a window
	editor.addButton('bigblock', {
		text: 'Блок "СТОРИ"',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Блок "СТОРИ"',
                minWidth: 800,
                // minHeight: 650,
                layout: 'flex',
                direction: 'column',
                align: 'stretch',
				body: [
                    {type: 'textbox', name: 'header', label: 'Заголовок блока', autofocus: true},
				    {type: 'textbox', name: 'title', label: 'Заголовок'},
                    {type: 'textbox', name: 'subtitle', label: 'Подзаголовок'},
                    {type: 'textbox', name: 'bloglink', label: 'Ссылка на статью'},
                    {
                        name: 'src',
                        type: 'filepicker',
                        // filetype: 'image',
                        label: 'Фото подложка',
                        onchange: srcChange,
                        onbeforecall: onBeforeCall
                    },
				],
				onsubmit: function(e) {

					var cont = '' +
                        '<section class="story main-article">' +
                        '<a href="' + e.data.bloglink + '" class="story__content inner-main" style="background-image: url('+ e.data.src + ');">\n' +
                        '<div class="story__box">\n' +
                        '<div class="story__subtitle">' + e.data.header + '</div>\n' +
                        '<h2 class="story__title">' + e.data.title + '</h2>\n' +
                        '<div class="story__text">' + e.data.subtitle + '</div>\n' +
                        '</div>\n' +
                        '</a></section>';

                    editor.insertContent(cont);
				}
			});
		}
	});

    function srcChange(e) {
        var srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

        if (imageListCtrl) {
            imageListCtrl.value(editor.convertURL(this.value(), 'src'));
        }

        tinymce.each(meta, function(value, key) {
            win.find('#' + key).value(value);
        });

        if (!meta.width && !meta.height) {
            srcURL = editor.convertURL(this.value(), 'src');

            // Pattern test the src url and make sure we haven't already prepended the url
            prependURL = editor.settings.image_prepend_url;
            absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
            if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
                srcURL = prependURL + srcURL;
            }

            this.value(srcURL);
        }
    }

    function onBeforeCall(e) {
        e.meta = win.toJSON();
    }
});