/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * bigvideo plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('bigvideo', function(editor, url) {
    function youtubeGetId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    // Add a button that opens a window
	editor.addButton('bigvideo', {
		text: 'Блок "ВИДЕО"',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Блок "ВИДЕО"',
                minWidth: 800,
                // minHeight: 650,
                layout: 'flex',
                direction: 'column',
                align: 'stretch',
				body: [
				    {type: 'textbox', name: 'title', label: 'Emoji'},
                    {type: 'textbox', name: 'youtubeUrl', label: 'URL видео в Youtube'},
                    {type: 'textbox', name: 'description', label: 'Название'},
				],
				onsubmit: function(e) {

					var cont = '<section class="video main-article"><div class="video-wrapper">' +
                        '        <h2 class="video_title">' +
                                e.data.title +
                        '        </h2>' +
                        '        <div class="video_frame-wrap">' +
                        '            <iframe class="video_frame" src="//www.youtube.com/embed/'+ youtubeGetId(e.data.youtubeUrl) +'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' +
                        '        </div>' +
                        '        <div class="video_description">' +
                                        e.data.description +
                        '        </div>' +
                        '    </div></section>';

                    editor.insertContent(cont);
				}
			});
		}
	});

    function srcChange(e) {
        var srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

        if (imageListCtrl) {
            imageListCtrl.value(editor.convertURL(this.value(), 'src'));
        }

        tinymce.each(meta, function(value, key) {
            win.find('#' + key).value(value);
        });

        if (!meta.width && !meta.height) {
            srcURL = editor.convertURL(this.value(), 'src');

            // Pattern test the src url and make sure we haven't already prepended the url
            prependURL = editor.settings.image_prepend_url;
            absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
            if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
                srcURL = prependURL + srcURL;
            }

            this.value(srcURL);
        }
    }

    function onBeforeCall(e) {
        e.meta = win.toJSON();
    }
});