/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*global tinymce:true */

tinymce.PluginManager.add('insertdatetime', function(editor) {


    function insertDateTime(format) {
        var html = getDateTime(format);

        if (editor.settings.insertdatetime_element) {
            var computerTime;

            if (/%[HMSIp]/.test(format)) {
                computerTime = getDateTime("%Y-%m-%dT%H:%M");
            } else {
                computerTime = getDateTime("%Y-%m-%d");
            }

            html = '<time datetime="' + computerTime + '">' + html + '</time>';

            var timeElm = editor.dom.getParent(editor.selection.getStart(), 'time');
            if (timeElm) {
                editor.dom.setOuterHTML(timeElm, html);
                return;
            }
        }

        editor.insertContent(html);
    }

    // editor.addCommand('mceInsertDate', function() {
    //     insertDateTime(editor.getParam("insertdatetime_dateformat", editor.translate("%Y-%m-%d")));
    // });
    //
    // editor.addCommand('mceInsertTime', function() {
    //     insertDateTime(editor.getParam("insertdatetime_timeformat", editor.translate('%H:%M:%S')));
    // });

    editor.addButton('insertdatetime', {
        type: 'splitbutton',
        title: 'Insert date/time',
        onclick: function() {
            insertDateTime(lastFormat || defaultButtonTimeFormat);
        },
        menu: menuItems
    });

    tinymce.each(editor.settings.insertdatetime_formats || [
        "%H:%M:%S",
        "%Y-%m-%d",
        "%I:%M:%S %p",
        "%D"
    ], function(fmt) {
        if (!defaultButtonTimeFormat) {
            defaultButtonTimeFormat = fmt;
        }

        menuItems.push({
            text: getDateTime(fmt),
            onclick: function() {
                lastFormat = fmt;
                insertDateTime(fmt);
            }
        });
    });

    editor.addMenuItem('insertdatetime', {
        icon: 'date',
        text: 'Date/time',
        menu: menuItems,
        context: 'insert'
    });
});
