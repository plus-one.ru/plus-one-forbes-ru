/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * threeBlock plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('threeBlock', function(editor, url) {
	// Add a button that opens a window
	editor.addButton('threeBlock', {
		text: 'Тройной блок',
		icon: false,
		onclick: function() {
            editor.windowManager.open({
                title: 'TinyMCE site',
                // url: url + '/dialog.html',
                // width: 980,
                // height: 400,

                	body: [{
                        type: 'container',
                        layout: 'stack',
                        id: 'common',
                        items: [{
                            type: 'buttongroup',
                            name: 'buttongroup',
                            label: 'buttongroup',
                            items: [
                                {
                                    text: 'С картинкой',
                                    value: 'button1',
                                    onclick: function (e) {
                                        $("#digit").hide();
                                        $("#image").show();
                                        $("#typeblock").val('image');
                                    }
                                },
                                {
                                    text: 'Цифра',
                                    value: 'button2',
                                    onclick: function (e) {
                                        $("#image").hide();
                                        $("#digit").show();
                                        $("#typeblock").val('digit');
                                    }
                                },
                                {
                                    type: 'textbox',
                                    id: 'typeblock',
                                    name: 'typeblock',
                                    value: 'image',
                                    hidden: true
                                }]
                        }]
                    },

                        {
                            type: 'container',
                            layout: 'stack',
                            id: 'colorShema',
                            items: [
                                {type: 'label', text: 'Цветовая схема'},
                                {
                                    type: 'listbox',
                                    name: 'color',
                                    label: 'Цвет',
                                    values: [
                                        {text: 'Еда', value: '#fc01ff'}, // main-article--black main-article--red-color--3
                                        {text: 'Арктика', value: '#303451'}, // main-article--d-yellow main-article--black-color--4
                                        {text: 'Реформа', value: '#3f3735'}, // style-blue main-article--yellow-4
                                        {text: 'Реформа 1', value: '#3f3735_1'}, // style-white main-article--persian-red

                                        // {text: 'Антибиотик', value: '#ff00ff'}, // main-article--magenta-reverse
                                        // {text: 'Старик', value: '#662d8e'}, // main-article--vivid-violet-reverse
                                        // {text: 'Подвиг', value: '#6947ff'}, // main-article--neon-reverse || main-article--neon
                                        // {text: 'Реформа', value: '#fe0000'}, // main-article--dark-red-reverse
                                        // // {text: 'Москва', value: '#cc00ff'}, // ???????
                                        // {text: 'Digitalis', value: '#cdffff'}, // main-article--cyan
                                        // {text: 'Зацикленный', value: '#cdff00'}, // main-article--lime-reverse
                                        // {text: 'Уголь', value: '#ff00ff_'}, // main-article--magenta-reverse (оказался таким же как в Антибиотике, но пусть будет, т.к. у этих схем различается цвет шрифта)
                                        // // {text: 'Арктика', value: '#d2c700'}, // main-article--yellow-reverse
                                        // // {text: 'Кудрин', value: '#ffffff'}, // ??????????
                                        // {text: 'Маркс', value: '#fff800'}, // main-article--yellow-reverse
                                    ]
                                }
                            ]
                        },

                    {
                        type: 'container',
                        layout: 'stack',
                        id: 'image',
                        items: [
                            {type: 'label', text: 'Фото подложка'},
                            {
                                name: 'src',
                                type: 'filepicker',
                                label: 'Фото подложка',
                                onchange: srcChange,
                                onbeforecall: onBeforeCall
                            },
                            {type: 'label', text: 'Заголовок'},
                            {type: 'textbox', name: 'title', label: 'Заголовок'},
                            {type: 'label', text: 'Ссылка'},
                            {type: 'textbox', name: 'url', label: 'Ссылка'}
                        ]
                    },
                    {
                            type: 'container',
                            layout: 'stack',
                            id: 'digit',
                            hidden: true,
                            style: 'left: 20px; top: 118px; width: 214px; height: 144px;',
                            items: [
                                {type: 'label', text: 'Цифра'},
                                {type: 'textbox', name: 'digit', label: 'Цифра'},
                                {type: 'label', text: 'Текст'},
                                {type: 'textbox', name: 'text', label: 'Текст'}
                            ]
                        }
                	],
                onsubmit: function(e) {

                    console.log(e.data);

                    var body = editor.getBody();
                    var item = body.getElementsByClassName('main-article__item');
                    if (item.length < 3){
                        if (item.length > 0){
                            var newSection = document.createElement('section');
                            newSection.className = 'main-article__item'

                            if (e.data.typeblock == 'image'){ // картинка

                                var newSection = document.createElement('section');
                                newSection.className = 'main-article__item'

                                var html = '<a class="main-article__box" href="' + e.data.url + '">\n' +
                                    '<div class="main-article__title-box same-height-left same-height-right">\n' +
                                    '<h1 class="main-article__title">\n' + e.data.title + '\n</h1>\n' +
                                    '</div>\n' +
                                    '<div class="main-article__img-box">\n' +
                                    '<picture>\n' +
                                    '<img src="' + e.data.src + '" alt="' + e.data.title + '" width="337" height="481">\n' +
                                    '</picture>\n' +
                                    '</div>\n'
                                '</a>\n';
                            }

                            if (e.data.typeblock == 'digit'){ // цифра

                                var newSection = document.createElement('section');
                                newSection.className = 'main-article__item'

                                var html = '<div class="main-article__box" style="height: 477px; margin-top: 47px;">\n' +
                                    '<div class="main-article__text-box main-article--columns same-height-left same-height-right" style="height: 477px;">\n' +
                                    '<div>\n' +
                                    '<p>' + e.data.digit + ' <br> ' + e.data.text + '</p>\n' +
                                    '</div>\n' +
                                    '</div>\n' +
                                    '</div>';
                            }
                            newSection.innerHTML = html;

                            item[0].parentNode.appendChild(newSection)
                        }
                        else{

                            var presetTemplate = getPresetColor(e.data.color);

                            var mainBlock = document.createElement('div');
                            mainBlock.className = 'main-article main-article--trio ' + presetTemplate.textClass + ' ' + presetTemplate.divClass;
                            // mainBlock.style = "height: 732px;"

                            var saveAsBlockLeft = document.createElement('div');
                            saveAsBlockLeft.className = "main-article__heading";
                            saveAsBlockLeft.innerHTML = "Save as";

                            var saveAsBlockRight = document.createElement('div');
                            saveAsBlockRight.className = "main-article__heading main-article__heading--2";
                            saveAsBlockRight.innerHTML = "Save as";

                            if (e.data.typeblock == 'image'){ // картинка

                                var newSection = document.createElement('section');
                                newSection.className = 'main-article__item'

                                var html = '<a class="main-article__box" href="' + e.data.url + '">\n' +
                                    '<div class="main-article__title-box same-height-left same-height-right">\n' +
                                    '<h1 class="main-article__title">\n' + e.data.title + '\n</h1>\n' +
                                    '</div>\n' +
                                    '<div class="main-article__img-box">\n' +
                                    '<picture>\n' +
                                    '<img src="' + e.data.src + '" alt="' + e.data.title + '" width="337" height="481">\n' +
                                    '</picture>\n' +
                                    '</div>\n'
                                '</a>\n';
                            }

                            if (e.data.typeblock == 'digit'){ // цифра

                                var newSection = document.createElement('section');
                                newSection.className = 'main-article__item'

                                var html = '<div class="main-article__box" style="height: 477px; margin-top: 47px;">\n' +
                                    '<div class="main-article__text-box main-article--columns same-height-left same-height-right" style="height: 477px;">\n' +
                                    '<div>\n' +
                                    '<p>' + e.data.digit + ' <br> ' + e.data.text + '</p>\n' +
                                    '</div>\n' +
                                    '</div>\n' +
                                    '</div>';
                            }

                            newSection.innerHTML = html;

                            mainBlock.append(saveAsBlockLeft);

                            mainBlock.append(newSection);

                            mainBlock.append(saveAsBlockRight);
                            body.append(mainBlock);

                            var itemP = body.getElementsByTagName('p');
                            itemP[0].remove();
                        }
                    }
                }
            });
		}
	});

    function getPresetColor(colorCode) {

        var preset = {

            '#fc01ff': {
                'divClass': 'main-article--black',
                'textClass': 'main-article--red-color--3'
            },
            '#303451': {
                'divClass': 'main-article--d-yellow',
                'textClass': 'main-article--black-color--4'
            },

            '#3f3735': {
                'divClass': 'main-article--yellow-4',
                'textClass': 'style-blue'
            },

            '#3f3735_1': {
                'divClass': 'main-article--persian-red',
                'textClass': 'style-black-2'
            },



            '#ff00ff': {
                'divClass': 'main-article--magenta-reverse',
                'textClass': 'style-blue-4 main-article--visteria'
            },
            '#662d8e': {
                'divClass': 'main-article--vivid-violet-reverse',
                'textClass': 'style-white main-article--persian-red'
            },
            '#6947ff': {
                'divClass': 'main-article--neon-reverse',
                'textClass': 'main-article--white style-dark-red'
            },
            '#fe0000': {
                'divClass': 'main-article--dark-red-reverse',
                'textClass': 'main-article--black main-article--red-color--3'
            },
            '#cdffff': {
                'divClass': 'main-article--cyan-reverse',
                'textClass': 'style-blue main-article--aqua'
            },
            '#cdff00': {
                'divClass': 'main-article--lime-reverse',
                'textClass': 'style-purple main-article--rosy'
            },
            '#ff00ff_': {
                'divClass': 'main-article--magenta-reverse',
                'textClass': 'style-blue-4 main-article--visteria'
            },
            '#d2c700': {
                'divClass': 'main-article--yellow-reverse',
                'textClass': 'main-article--d-yellow main-article--black-color--4'
            },
            '#fff800': {
                'divClass': 'main-article--yellow-reverse',
                'textClass': 'main-article--red'
            }
        };

        return preset[colorCode];
    }

    function srcChange(e) {
        var srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

        if (imageListCtrl) {
            imageListCtrl.value(editor.convertURL(this.value(), 'src'));
        }

        tinymce.each(meta, function(value, key) {
            win.find('#' + key).value(value);
        });

        if (!meta.width && !meta.height) {
            srcURL = editor.convertURL(this.value(), 'src');

            // Pattern test the src url and make sure we haven't already prepended the url
            prependURL = editor.settings.image_prepend_url;
            absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
            if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
                srcURL = prependURL + srcURL;
            }

            this.value(srcURL);
        }
    }

    function onBeforeCall(e) {
        e.meta = win.toJSON();
    }
});