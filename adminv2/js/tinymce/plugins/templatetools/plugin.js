/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*global tinymce:true */

tinymce.PluginManager.add('templatetools', function(editor) {
	let lastSelectedTemplate = false;
	function addButtons() {
		editor.addButton('fixedAllTemplate', {
			title: 'Заблокировать все темплейты для возвожности (перемещения, удаления) шаблонов вставок',
			text: 'Заблокировать All Templates',
			cmd: 'mceTemplateFixedAll'
		});
		editor.addButton('freeAllTemplate', {
			title: 'Освободить все темплейты ',
			text: 'Освободить All Templates',
			cmd: 'mceTemplateFreeAll'
		});
		editor.addButton('fixedTemplate', {
			title: 'Заблокировать',
			text: 'Заблокировать',
			cmd: 'mceTemplateFixed'
		});
		editor.addButton('freeTemplate', {
			title: 'Освободить',
			text: 'Освободить',
			cmd: 'mceTemplateFree'
		});
		editor.addButton('deleteTemplate', {
			title: 'Удалить',
			text: 'Удалить',
			cmd: 'mceTemplateDelete'
		});
	}

	function fixedTemplate() {
		return function () {
			lastSelectedTemplate.contentEditable = false;
		};
	}

	function freeTemplate() {
		return function () {
			lastSelectedTemplate.contentEditable = true;
		};
	}

	function fixedAllTemplate() {
		return function () {
			editor.dom.select(editor.settings.templatetools_settings.templateSelectors.join(',')).forEach(function (templ) {
				templ.contentEditable = false;
			});
		};
	}

	function freeAllTemplate() {
		return function () {
			editor.dom.select(editor.settings.templatetools_settings.templateSelectors.join(',')).forEach(function (templ) {
				templ.contentEditable = true;
			});
		};
	}

	function removeTemplate() {
		return function () { lastSelectedTemplate.remove(); };
	}

	function replaceTemplateDialog() {
		return false;
	}

	function addEvents() {
		editor.on('Blur', function(e) {
			setTimeout(function(){
				Array.from(e.target.contentDocument.getElementsByClassName('selected--template')).forEach(function (elRemClass) {
					elRemClass.classList.remove('selected--template');
				});
			}, 0);
		});
		editor.on('NodeChange', function(e) {
			if(lastSelectedTemplate) {
				lastSelectedTemplate.classList.remove('selected--template');
			}
			lastSelectedTemplate = false;
			//Set up the lastSelectedTemplate
			let isEditable = isEditableTemplate(e.element);
			if (!isEditable) {
				e.parents.forEach(function (el, i) {
					if (lastSelectedTemplate) { return ;}
					if (editor.dom.is(el, editor.settings.templatetools_settings.templateSelectors.join(',')) ) {
						lastSelectedTemplate = el;
					}
				});
			} else {
				lastSelectedTemplate = e.element;
			}
			if (lastSelectedTemplate) {
				lastSelectedTemplate.classList.add('selected--template');
			}
		});
	}

	function isEditableTemplate(eventElement) {
		return editor.dom.is(eventElement, editor.settings.templatetools_settings.templateSelectors.join(','));
	}

	function addToolbars() {
		var toolbarItems = false;

		if (!toolbarItems) {
			toolbarItems = 'fixedTemplate | freeTemplate | deleteTemplate';
		}

		editor.addContextToolbar(
			isEditableTemplate,
			toolbarItems
		);
	}

	tinymce.util.Tools.each({
		mceTemplateFixedAll: fixedAllTemplate(),
		mceTemplateFreeAll: freeAllTemplate(),
		mceTemplateFixed: fixedTemplate(),
		mceTemplateFree: freeTemplate(),
		mceTemplateReplace: replaceTemplateDialog,
		mceTemplateDelete: removeTemplate(),
	}, function(fn, cmd) {
		editor.addCommand(cmd, fn);
	});


	addButtons();
	addToolbars();
	addEvents();
});
