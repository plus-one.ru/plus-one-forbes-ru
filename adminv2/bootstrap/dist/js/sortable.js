/**
 * Configure the elements in the SortableElements div to be sortable with jQuery
 * @returns void
 */
function initSortableContainer()
   {
   $(function()
      {
      $("#SortableElements").sortable(
          {
          cursor: "move",
          stop: handleReorderElements
          });
      } );
   }

/**
 * Posts an array of element Id's to the PHP controller via AJAX
 * @returns void
 */
function handleReorderElements()
   {
   var url = 'controllers/SortController.php';
   var fieldData = $( "#SortableElements" ).sortable( "serialize" );
   fieldData += "&action=reorderElements";

   var posting = $.post( url, fieldData);
   posting.done( function( data)
      {
      reorderElementsResponse( data );
      });
   }

/**
 * Handles the AJAX response. For most applications, you only have to handle any returned error messages,
 * but for this example, we're returning information about the sort operation for display.
 * @param {JSON} data
 * @returns void
 */
function reorderElementsResponse( data )
   {
   if (data.FAIL === undefined) // Everything's cool!
      {
      $("#Message").html( data.resultString + data.itemIndexString );
      }
   else
      {
      alert( "Bad Clams!" );
      }
   }
