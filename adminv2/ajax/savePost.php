<?php
	session_start();
	chdir('..'); 
//	require_once('Widget.admin.php');
//    $widget = new Widget();


    require_once('BlogPosts.admin.php');
    $post = new BlogPosts();
    $result = $post->savePost();


    // по умолчанию флаг ошибки в положении "нет ошибок"
    $error = false;
    $errorMessage = array();

	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($result);
?>