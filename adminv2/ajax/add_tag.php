<?php
	session_start();
	chdir('..'); 
	require_once('Widget.admin.php');
    $widget = new Widget();
    require_once('PostTags.admin.php');
    $postTag = new PostTags();

    // по умолчанию флаг ошибки в положении "нет ошибок"
    $error = false;
    $errorMessage = array();

    $tag = $_POST['tag'];
    $itemId = $_POST['itemId'];

    // проверка на заполненность обязательных полей
    if ($tag==""){
        $error = true;
        $errorMessage[] = 'Заполните все обязательные поля. Они отмечены знаком звездочки *.';
    }

    // если ошибок в заполнении не обнаружено - то приступаем к сохранению товара в магазине
    if ($error===false){
        // данные для сохранения в каталог магазина
        $saveData = new stdClass();

        $result = $postTag->saveTag($tag);

        if ($result === true) {
            $return['success'] = 1;

            $availableTags = $postTag->getTags();

            if (isset($itemId)){
                $availableTags = $postTag->getRelations($availableTags, $itemId);
            }

            $widget->smarty->assign('availableTags', $availableTags);
            $result = $widget->smarty->fetch('include/include_post_tags.tpl');

        } else {
            $return['error'] = 1;
            $return['errorMessage'] = "Произошла внутренняя ошибка при сохранении товара. Обратитесь к разработчику.";
        }
    }
    else{
        $return['error'] = 1;
        $return['errorMessage'] = $errorMessage;
    }

	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($result);
?>