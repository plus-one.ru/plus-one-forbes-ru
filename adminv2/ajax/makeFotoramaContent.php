<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
chdir('../');

require_once('Widget.admin.php');

$widget = new Widget();

$galleryId = $_GET['galleryId'];


$result = '';

if (!empty($galleryId)){

    $query = sql_placeholder('SELECT * FROM images WHERE gallery_id=? ORDER BY order_num DESC', $galleryId);
    $widget->db->query($query);
    $imagesList = $widget->db->results();

    $widget->smarty->assign('imagesList', $imagesList);

    $result = $widget->smarty->fetch('include/galleryImagesList.tpl');

}

header("Content-type: text/html; charset=UTF-8");
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print $result;
