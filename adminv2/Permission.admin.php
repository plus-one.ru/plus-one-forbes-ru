<?PHP
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class Permission
 */
class Permission extends Widget
{
    var $item;
    var $salt = 'simpla'; // Соль для шифрования пароля
    private $tableName = 'users_admin';
  
    function Permission(&$parent = null){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));

        if(isset($_POST['name_user'])){
            $this->check_token();

            $this->item->name = $_POST['name_user'];
            $this->item->login = $_POST['usrl'];
            // надо выяснить изменился ли пароль у пользователя.
            if ($_POST['usrp'] != ''){
                $this->item->password = $_POST['usrp'];
                // шифруем пароль
                $this->item->password = md5($this->item->password.$this->salt);
            }

            $this->item->enabled = 0;
            if(isset($_POST['enabled']) && $_POST['enabled']==1) {
                $this->item->enabled = 1;
            }

            ## Не допустить одинаковые URL статей
    	    $query = sql_placeholder('SELECT COUNT(*) AS count FROM ' . $this->tableName . ' WHERE login=? AND id!=?', $this->item->login, $item_id);
            $this->db->query($query);
            $res = $this->db->result();

  		    if(empty($this->item->name)){
			    $this->error_msg = $this->lang->ENTER_TITLE;
  		    }
  		    elseif($res->count>0){
			    $this->error_msg = 'Пользователь с таким логином уже существует. Укажите другое.';
  		    }
            else{
  			    if(empty($item_id)) {
                    $this->item->created = date('Y-m-d H:i:s', time());
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }
  	    		else{
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    if (is_null($this->update_article($item_id))){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }

                $get = $this->form_get(array('section'=>'Permissions'));

                if (empty($this->error_msg)){

                    // пропишем доступ пользователю на модули
                    $this->setModulesPermission($_POST['modules'], $item_id);

                    if(isset($_GET['from'])){
                        header("Location: ".$_GET['from']);
                    }
                    else{
                        header("Location: index.php$get");
                    }
                }
  		    }
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новый пользователь';
		}
		else{
			$this->title = 'Изменить пользователя: ' . $this->item->name . ' (' . $this->item->login . ')';
		}

        $query = sql_placeholder("SELECT * FROM modules WHERE type_module='admin' ORDER BY order_num ASC");
        $this->db->query($query);
        $modules = $this->db->results();

        // уже отобранные модули
        $query = sql_placeholder('SELECT module_id FROM users_admin_permissions WHERE user_id=?',$this->item->id);
        $this->db->query($query);
        $selectedModules = $this->db->results();

        foreach ($selectedModules AS $selectedModule){
            $selectedModules[$selectedModule->module_id] = $selectedModule->module_id;
        }

        foreach ($modules AS $key=>$module){
            if (in_array($module->module_id, (array)$selectedModules)){
                $modules[$key]->check = 1;
            }
            else{
                $modules[$key]->check = 0;
            }
        }

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
        $this->smarty->assign('modules', $modules);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);

		$this->body = $this->smarty->fetch('permission.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);
        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }

    function setModulesPermission($modules, $userId){

        // очищаем таблицу от старых разрешений
        $query = sql_placeholder('DELETE FROM users_admin_permissions WHERE user_id=?', $userId);
        $this->db->query($query);

        foreach ($modules AS $moduleId=>$v){
            $query = sql_placeholder('INSERT INTO users_admin_permissions SET user_id=?, module_id=?', $userId, $moduleId);
            $this->db->query($query);
        }
    }
}
