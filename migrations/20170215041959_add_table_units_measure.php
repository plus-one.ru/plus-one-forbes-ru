<?php

use Phinx\Migration\AbstractMigration;

class AddTableUnitsMeasure extends AbstractMigration
{
    private $tablename = 'units_measure';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('name', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('short_name', 'string', ['limit'=>255, 'null' => false, 'default'=>0])
            ->addColumn('enabled', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('order_num', 'integer', ['limit'=>11, 'null' => false, 'default' => 1])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
