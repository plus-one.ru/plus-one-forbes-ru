<?php

use Phinx\Migration\AbstractMigration;

class AddTableWriters extends AbstractMigration
{
    private $tablename = 'blogwriters';

    public function up()
    {
        $this->table($this->tablename)
                ->addColumn('name', 'string', ['limit'=>255, 'null' => false])
                ->addColumn('image', 'string', ['limit'=>255, 'null' => false])
                ->addColumn('enabled', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
                ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
