<?php

use Phinx\Migration\AbstractMigration;

class AlterTableModulesAddIcon extends AbstractMigration
{
    private $tablename = 'modules';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('icon', 'string', ['limit' => 255, 'null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('icon')
            ->save();
    }
}
