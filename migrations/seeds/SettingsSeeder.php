<?php

use Phinx\Seed\AbstractSeed;

class SettingsSeeder extends AbstractSeed
{
    private $tablename = 'settings';

    public function run()
    {
        $data = array(
            array('name'=>'company_name', 'name_cyr'=>'Название компании', 'description'=>'', 'value'=>'Коммерсант +1', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'1', ),
            array('name'=>'article_path_to_uploaded_images', 'name_cyr'=>'Путь для хранения картинок', 'description'=>'', 'value'=>'files/blog/', 'type_value'=>'urltext', 'group_id'=>'2', 'order_num'=>'1', ),
            array('name'=>'main_section', 'name_cyr'=>'Главная страница сайта', 'description'=>'', 'value'=>'mainpage', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'2', ),
            array('name'=>'article_large_image_width', 'name_cyr'=>'Ширина большой картинки', 'description'=>'', 'value'=>'800', 'type_value'=>'text', 'group_id'=>'2', 'order_num'=>'2', ),
            array('name'=>'site_name', 'name_cyr'=>'Название сайта', 'description'=>'', 'value'=>'Коммерсант +1', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'3', ),
            array('name'=>'article_large_image_height', 'name_cyr'=>'Высота большой картинки', 'description'=>'', 'value'=>'333', 'type_value'=>'text', 'group_id'=>'2', 'order_num'=>'3', ),
            array('name'=>'admin_email', 'name_cyr'=>'Email администратора сайта', 'description'=>'', 'value'=>'admin@site.ru', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'4', ),
            array('name'=>'article_small_image_width', 'name_cyr'=>'Ширина маленькой картинки', 'description'=>'', 'value'=>'150', 'type_value'=>'text', 'group_id'=>'2', 'order_num'=>'4', ),
            array('name'=>'article_small_image_height', 'name_cyr'=>'Высота маленькой картинки', 'description'=>'', 'value'=>'75', 'type_value'=>'text', 'group_id'=>'2', 'order_num'=>'5', ),
            array('name'=>'site_url', 'name_cyr'=>'URL адрес сайта', 'description'=>'', 'value'=>'', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'8', ),
            array('name'=>'theme', 'name_cyr'=>'Тема (оформление) фронтенда сайта', 'description'=>'', 'value'=>'plusone', 'type_value'=>'text', 'group_id'=>'1', 'order_num'=>'9', ),
            array('name'=>'meta_autofill', 'name_cyr'=>'Автоматическое заполнение метатегов', 'description'=>'', 'value'=>'1', 'type_value'=>'checkbox', 'group_id'=>'1', 'order_num'=>'10', ),
        );

        $table = $this->table($this->tablename);
        $table->insert($data)->save();
    }
}
