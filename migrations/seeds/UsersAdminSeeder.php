<?php

use Phinx\Seed\AbstractSeed;

class UsersAdminSeeder extends AbstractSeed
{
    private $tablename = 'users_admin';

    public function run()
    {
        $data = array(
            array(
                'login' => 'admin',
                'password' => '1c78228d037863611a46cc92460127a3', // XomA101278
                'enabled' => 1,
                'created'=>'1970-01-01 00:00:01',
                'modified'=>'1970-01-01 00:00:01'
            ),
        );

        $table = $this->table($this->tablename);
        $table->insert($data)->save();
    }
}
