<?php

use Phinx\Seed\AbstractSeed;

class ModulesSeeder extends AbstractSeed
{
    private $tablename = 'modules';

    public function run()
    {
        $data = array(
            array(
                'class' => 'StaticPage',
                'name' => 'Статическая страница',
                'valuable' => 1,
                'url' => ''
            ),
            array(
                'class' => 'Blog',
                'name' => 'Блог',
                'valuable' => 0,
                'url' => ''
            ),
        );

        $table = $this->table($this->tablename);
        $table->insert($data)->save();
    }
}
