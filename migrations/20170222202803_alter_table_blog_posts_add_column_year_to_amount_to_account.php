<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnYearToAmountToAccount extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('year_to_ammount', 'string', ['limit'=> 4, 'null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('year_to_ammount')
            ->save();
    }
}
