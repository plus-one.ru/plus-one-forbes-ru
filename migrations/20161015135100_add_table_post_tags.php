<?php

use Phinx\Migration\AbstractMigration;

class AddTablePostTags extends AbstractMigration
{
    private $tablename = 'post_tags';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('url', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('name', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('enabled', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->addIndex('url', array('unique'=>true))
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
