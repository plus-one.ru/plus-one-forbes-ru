<?php

use Phinx\Migration\AbstractMigration;

class AddTableImages extends AbstractMigration
{
    private $tablename = 'images';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('filename', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('gallery_id', 'integer', ['null' => false])
            ->addColumn('image_name', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('image_author', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('enabled', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('order_num', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
