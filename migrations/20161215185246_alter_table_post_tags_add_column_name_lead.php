<?php

use Phinx\Migration\AbstractMigration;

class AlterTablePostTagsAddColumnNameLead extends AbstractMigration
{
    private $tablename = 'post_tags';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('name_lead', 'string', ['limit' => 255, 'null' => false])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('name_lead')
            ->save();
    }
}
