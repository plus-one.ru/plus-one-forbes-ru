<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsAddConference extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('conference', 'integer', ['null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('conference')
            ->save();
    }
}
