<?php

use Phinx\Migration\AbstractMigration;

class AddColumnLeadToBlogposts extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('lead', 'text', ['null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('lead')
            ->save();
    }
}
