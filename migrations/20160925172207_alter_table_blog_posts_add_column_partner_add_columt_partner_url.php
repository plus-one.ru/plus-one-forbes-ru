<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnPartnerAddColumtPartnerUrl extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('partner', 'integer', ['limit'=>11, 'null' => true, 'default' => 0])
            ->addColumn('partner_url', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('partner')
            ->removeColumn('partner_url')
            ->save();
    }
}
