<?php

use Phinx\Migration\AbstractMigration;

class CreateTableAbout extends AbstractMigration
{
    private $tablename = 'about';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('header', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('body', 'text', ['null' => true])
            ->addColumn('image_name', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
