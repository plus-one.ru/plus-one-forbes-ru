<?php

use Phinx\Migration\AbstractMigration;

class AddTableSections extends AbstractMigration
{
    private $tablename = 'sections';

    public function up()
    {
        $this->table($this->tablename, array('id' => 'section_id'))
            ->addColumn('url', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('parent', 'integer', ['null' => true, 'default' => null])
            ->addColumn('name', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('header', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('meta_title', 'string', ['limit'=>255, 'null' => false, 'default' => null])
            ->addColumn('meta_description', 'string', ['limit'=>500, 'null' => false, 'default' => null])
            ->addColumn('meta_keywords', 'string', ['limit'=>500, 'null' => false, 'default' => null])
            ->addColumn('body', 'text', ['null' => false, 'default' => null])
            ->addColumn('menu_id', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('order_num', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('module_id', 'integer', ['limit'=>11, 'null' => true, 'default' => null])
            ->addColumn('enabled', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
