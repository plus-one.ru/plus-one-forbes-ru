<?php

use Phinx\Migration\AbstractMigration;

class AlterTablePostTagsAddColumnParent extends AbstractMigration
{
    private $tablename = 'post_tags';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('parent', 'integer', ['null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('parent')
            ->save();
    }
}
