<?php

use Phinx\Migration\AbstractMigration;

class AddTablePostTagsRelations extends AbstractMigration
{
    private $tablename = 'relations_post_tags';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('tag_id', 'integer', ['limit'=>11, 'null' => false])
            ->addColumn('post_id', 'integer', ['limit'=>11, 'null' => false])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
