<?php

use Phinx\Migration\AbstractMigration;

class AddColumtColorToPartners extends AbstractMigration
{
    private $tablename = 'partners';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('color', 'string', ['limit' => 7, 'null' => true, 'default' => "#D2D2D2"])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('color')
            ->save();
    }
}
