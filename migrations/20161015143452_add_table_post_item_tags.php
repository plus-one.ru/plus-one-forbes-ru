<?php

use Phinx\Migration\AbstractMigration;

class AddTablePostItemTags extends AbstractMigration
{
    private $tablename = 'relations_postitem_tags';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('posttag_id', 'integer', ['limit'=>11, 'null' => false])
            ->addColumn('post_id', 'integer', ['limit'=>11, 'null' => false])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
