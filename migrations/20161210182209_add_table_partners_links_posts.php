<?php

use Phinx\Migration\AbstractMigration;

class AddTablePartnersLinksPosts extends AbstractMigration
{
    private $tablename = 'partners_posts_links';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('post_id', 'integer', ['null' => false])
            ->addColumn('header', 'text', ['null' => false])
            ->addColumn('subheader', 'text', ['null' => true, 'default' => null])
            ->addColumn('rubrika_id', 'integer', ['null' => false])
            ->addColumn('partner_id', 'integer', ['null' => true, 'default' => 0])
            ->addColumn('link', 'text', ['null' => false])
            ->addColumn('image', 'string', ['limit' => 255, 'null' => false])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
