<?php

use Phinx\Migration\AbstractMigration;

class AddTableSettings extends AbstractMigration
{
    private $tablename = 'settings';

    public function up()
    {
        $this->table($this->tablename, array('id' => 'setting_id'))
            ->addColumn('name', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('name_cyr', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('description', 'text', ['null' => false])
            ->addColumn('value', 'text', ['null' => false])
            ->addColumn('type_value', 'string', ['limit' => 200, 'default' => 0])
            ->addColumn('group_id', 'integer', ['default' => 0])
            ->addColumn('order_num', 'integer', ['default' => 0])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
