<?php

use Phinx\Migration\AbstractMigration;

class AlterTableConferencesAddColumnFontColor extends AbstractMigration
{
    private $tablename = 'conferences';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('font_color', 'string', ['limit' => 6, 'null' => true])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('font_color')
            ->save();
    }
}
