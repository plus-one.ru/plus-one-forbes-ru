<?php

use Phinx\Migration\AbstractMigration;

class AddTableBlogPost extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('url', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('tags', 'integer', ['null' => true, 'default' => null])
            ->addColumn('name', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('header', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('meta_title', 'string', ['limit'=>255, 'null' => false, 'default' => null])
            ->addColumn('meta_description', 'string', ['limit'=>500, 'null' => false, 'default' => null])
            ->addColumn('meta_keywords', 'string', ['limit'=>500, 'null' => false, 'default' => null])
            ->addColumn('body', 'text', ['null' => false, 'default' => null])
            ->addColumn('order_num', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('image', 'string', ['limit'=>255, 'null' => false, 'default' => ''])
            ->addColumn('writers', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('blocks', 'string', ['limit'=>10, 'null' => false, 'default' => ''])
            ->addColumn('enabled', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
