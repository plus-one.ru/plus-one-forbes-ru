<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsChangeColumnHeader extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->changeColumn('header', 'text', ['null' => false])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->changeColumn('header', 'string', ['limit'=>255, 'null' => false])
            ->save();
    }
}
