<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsAddColumnBlocksAuthor extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('blocks_author', 'string', ['limit' => 3, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('blocks_author')
            ->save();
    }
}
