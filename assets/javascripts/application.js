var application = (function () {
  //@prepros-prepend libs/jquery.min.js
  //@prepros-prepend libs/retina-cover.min.js
  //@prepros-prepend libs/is-mobile.min.js

  // SERVICE VARIABLES
  var isDevEnv =
      location.hostname.indexOf("local") !== -1 || location.port == 81,
    // var isDevEnv = location.hostname.indexOf('local') !== -1 || location.port == 81,

    //     apiUrl = isDevEnv ? 'http://91.240.87.34' : location.origin,
    apiUrl = location.origin,
    variables = {
      articleScroll: true,
      articlesScroll: true,
      nextArticles: null,
      relatedArticles: null,
      watchedArticles: [],
      currentArticle: 0,
      searchQuery: "",
      searchTags: "",
      searchPage: "",
      nextSearchPage: "",
      nextSlug: "",
      nextTitle: "",
      nextMetas: "",
    },
    selectors = {
      article: "#article-single",
      articleContent: "#article-single .article-content",
      articles: "#articles-list",
      metas:
        "meta[name=keywords], meta[name=description], meta[property^=og], meta[name^=twitter], link[rel=image_src]",
      search: ".plusone-form-input",
      searchWrap: ".js-search",
      overlay: ".js-overlay",
      articlesWrap: "#articles-wrap",
    },
    scrollPositionArticles = [];
  var scrollChangeUrlTimer = null;

  // HELPERS

  function parseURL(url) {
    var link = document.createElement("a");
    link.href = url;
    return link;
  }

  function present(variable) {
    return (
      variable && variable !== "" && variable !== null && variable !== undefined
    );
  }

  // GET DATA FROM API

  function getArticles(url, asNewList) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        var response = JSON.parse(this.responseText);
        showArticlesList(response, asNewList);
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  }

  function getArticlesByTag(url, historyUrl) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        $(".bg-stretch").retinaCover();

        // Change selectors
        $("body").removeClass("plusone-digest").addClass("plusone-page");
        let $jsContent = $(".js-content");
        if ($jsContent.find("#setka-editor").length === 0) {
          $jsContent
            .find(selectors.articles)
            .append('<div id="setka-editor" class="plusone-container"></div>');
        }
        $jsContent.find("#setka-editor").html(this.responseText);
        $jsContent.fadeIn(200).removeClass("hidden");
        // history.pushState(null, null, historyUrl);
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  }

  function getArticle(slug, isAdding, showPrev, showNext, historySave = true) {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        let response = JSON.parse(this.responseText);
        // console.log('application.js.getArticle response =', response)
      
        isAdding
          ? addArticle(response, slug)
          : showSingleArticle(response, slug, showPrev, showNext, historySave);
      }
    };
    xhttp.open(
      "GET",
      apiUrl + "/api/getpost/" + slug + (isAdding ? "?scrollPost=1" : ""),
      true
    );
    xhttp.setRequestHeader("Content-Type", "application/json");
  
    xhttp.send();
  }

  function searchArticles(asNewList) {
    var xhttp = new XMLHttpRequest(),
      searchParams = {};

    if (present(variables.searchQuery))
      searchParams["query"] = decodeURI(variables.searchQuery);
    if (present(variables.searchTags))
      searchParams["tags"] = variables.searchTags;
    if (present(variables.searchPage))
      searchParams["page"] = variables.searchPage;

    xhttp.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        setSearchContext();
        setSearchHistory();
        var response = JSON.parse(this.responseText);
        showArticlesList(response, asNewList);
        variables.nextSearchPage = response["_meta"]["next_page"];
      }
    };
    xhttp.open("POST", apiUrl + "/api/search");
    // xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(
      JSON.stringify({
        search: searchParams,
      })
    );
  }

  // OVERLAY

  function toggleOverlay() {
    $(selectors.overlay).toggleClass("opened").slideToggle();
  }

  // SEARCH

  function searchInputAutoSize() {
    var minInputWidth = 275;
    var margin = 80; // type sensitivity
    var contextFormWidth =
      document.getElementsByClassName("js-context-form").length > 0
        ? document.getElementsByClassName("js-context-form")[0].offsetWidth
        : 0;
    var contextSearch = document.getElementById("overlay-context-search");
    var fakeEl;
    var currentFakeEl = document.getElementsByClassName("js-measurer")[0];
    if (currentFakeEl) {
      fakeEl = currentFakeEl;
    } else {
      fakeEl = document.createElement("span");
      fakeEl.className = "plusone-form-input-measurer js-measurer";
      document.body.appendChild(fakeEl);
    }
    if (contextSearch) {
      fakeEl.innerText = contextSearch.value;
      if (fakeEl.offsetWidth > minInputWidth) {
        if (fakeEl.offsetWidth + margin < contextFormWidth) {
          contextSearch.style.width = fakeEl.offsetWidth + margin + "px";
        } else {
          contextSearch.style.width = "100%";
        }
      } else {
        contextSearch.style.width = minInputWidth + "px";
      }
    }
  }

  function resetSearchContext() {
    $(selectors.search).val("");
    $(selectors.searchWrap).hide().removeClass("visible");
    document.title = $("title").data("title");

    setInterval(function () {
      var currentFakeEl = document.getElementsByClassName("js-measurer")[0];

      if (currentFakeEl) currentFakeEl.innerHTML = "";

      searchInputAutoSize();
    }, 100);
  }

  function setSearchContext() {
    var tags = variables.searchTags,
      tags_str = "",
      tags_cnt = 0;

    if (tags !== "") {
      tags_cnt = tags.split(";").length;

      if (tags_cnt > 1) tags_str = "tags: " + tags;
      else if (tags_cnt === 1) tags_str = "tag: " + tags;
    }

    $(selectors.search).each(function () {
      var $el = $(this),
        val = $el.val();

      $el.val(val + "" + tags_str);
    });
  }

  function setSearchHistory() {
    var query = variables.searchQuery,
      tags = variables.searchTags,
      path = "search?";

    if (query !== "" && tags !== "") path += "query=" + query + "&tags=" + tags;
    else if (query !== "") path += "query=" + query;
    else if (tags !== "") path += "tags=" + tags;

    history.pushState(null, null, path);
    window.dispatchEvent(new Event("locationchange"));
  }

  // DYNAMIC CONTENT

  function initialContent(historySave = true) {
    var path = location.pathname,
      params = location.search.replace("?query=", ""),
      mainPage = $.inArray(path, ["", "/", "/index.html", undefined]) !== -1,
      firstPageTag =
        $.inArray(path, ["", "/", "/index.html", undefined]) !== -1;
  
    // console.log('application.js.initialContent path =', path)
    if (path === '/217471-8.html') {
      return
    }
    
    if (mainPage) {
      $(selectors.articles).html("");
      getArticles(apiUrl + "/api/articles", true);
      if (historySave) {
        history.pushState(null, null, location.pathname);
        window.dispatchEvent(new Event("locationchange"));
      }
    } else {
      if (path.indexOf("search") !== -1) {
        variables.searchQuery = params;
        $(selectors.searchWrap).show().addClass("visible");
        $(selectors.search).val(decodeURI(variables.searchQuery));
        searchArticles(true);
      } else if (path.match(/(ecology|society|economy)/)) {
        getArticlesByTag(
          apiUrl + "/api/articlesByTag" + location.pathname,
          location.pathname
        );
      } else {
        var postSlug = path.replace("/blog/", "");
        getArticle(postSlug, null, null, null, false);
      }
    }
    initialJqueryMain();
  }

  function showArticlesList(data, replace) {
    var $articlesBlock = $(selectors.articles);

    if ($articlesBlock.children("#setka-editor").length === 0) {
      $articlesBlock.append(
        data["_meta"]["setka_style"],
        '<div id="setka-editor" class="plusone-container"></div>'
      );
    }

    if (replace) {
      $articlesBlock.children("#setka-editor").html("");
    }

    if (data["posts"] && data["posts"].length) {
      data["posts"].forEach(function (item) {
        try {
          $articlesBlock.children("#setka-editor").append(item["content"]);
        } catch (e) {
          console.error("Error content:", item);
        }
      });
    }

    $(".bg-stretch").retinaCover();

    $("body")
      .removeClass("plusone-page")
      .addClass("plusone-digest")
      .css("padding-right", 0);

    $(selectors.article).hide();

    // Set title, meta and history
    document.title = data.title;
    $("head").find(selectors.metas).remove().end().append(data.ogMeta);

    $articlesBlock.show(10, function () {
      if (replace) $articlesBlock.scrollTop(0);
      $(".js-content").fadeIn(200).removeClass("hidden");
      $articlesBlock.css("overflow-y", "scroll");
    });

    if (data["_meta"]["total_pages"] > data["_meta"]["current_page"]) {
      variables.nextArticles = data["_meta"]["next_page"];
    } else {
      variables.nextArticles = null;
    }
  }

  function showSingleArticle(data, slug, prev, next, historySave = true) {
    scrollPositionArticles = [];
    // Append article content
    $(selectors.article)
      .find(".js-infinity-scroll")
      .html(
        '<div class="container" data-id="' +
          data.id +
          '" id="post_' +
          data.id +
          '"><div class="article"><div class="article__content">' +
          data["content"] +
          "</div></div></div>"
      );

    // Set related articles
    if (variables.relatedArticles == null) {
      if (data["relatedPosts"] && data["relatedPosts"].length > 0) {
        variables.relatedArticles = data["relatedPosts"];
        $(selectors.articleContent).css("padding-bottom", "150px");
      } else {
        variables.relatedArticles = [];
        $(selectors.articleContent).css("padding-bottom", 0);
      }
    }

    if (present(prev)) variables.currentArticle--;
    else if (present(next)) variables.currentArticle++;

    // Set title, meta and history
    document.title = data.title;
    $("head").find(selectors.metas).remove().end().append(data.metaTitle);
    if (historySave) {
      window.dispatchEvent(new Event("locationchange"));
    }

    // Adjust images for retina displays
    $(".bg-stretch").retinaCover();

    // Change selectors
    $("body").removeClass("plusone-digest").addClass("plusone-page");

    // Hide articles container
    $(selectors.articles).hide();

    // Show single article container
    $(selectors.article).show(10, function () {
      $(selectors.article).scrollTop(1);
      scrollPositionArticles.push({
        id: data.id,
        slug: slug,
        meta: data.metaTitle,
        title: data.title,
        _elem: $(selectors.article).find("#post_" + data.id),
      });
      $(".js-content").fadeIn(200).removeClass("hidden");
      $(selectors.article).css("overflow-y", "scroll");
    });
  }

  function addArticle(data, slug, historySave = true) {
    $(selectors.article)
      .find(".js-infinity-scroll")
      .append(
        '<div class="container" data-id="' +
          data.id +
          '" id="post_' +
          data.id +
          '"><div class="article"><div class="article__content">' +
          data["content"] +
          "</div></div></div>"
      );
    $(".bg-stretch").retinaCover();

    variables.nextSlug = slug;
    variables.nextTitle = data.title;
    variables.nextMetas = data.metaTitle;

    scrollPositionArticles.push({
      id: data.id,
      slug: slug,
      meta: data.metaTitle,
      title: data.title,
      _elem: $(selectors.article).find("#post_" + data.id),
    });
  }

  function resetState() {
    variables.articleScroll = true;
    variables.articlesScroll = true;
    variables.nextArticles = null;
    variables.relatedArticles = null;
    variables.watchedArticles = [];
    variables.currentArticle = 0;
    variables.searchQuery = "";
    variables.searchTags = "";
    resetSearchContext();
  }

  function resetAndShowArticles() {
    setTimeout(function () {
      resetState();
      $(selectors.article).find(".js-infinity-scroll").empty();

      getArticles(apiUrl + "/api/articles", true);
    }, 200);
  }

  //  MAIN BINDINGS

  $(function () {
    var searchDelay = null,
      $body = $("body");

    initialContent();

    if (!isMobile.any) {
      $(selectors.articles).addClass("with-custom-scrollbar");
      $(selectors.article).addClass("with-custom-scrollbar");
    }

    $body.on("click", ".js-overlay-trigger", toggleOverlay);
    $body.on("click", ".js-context-form-reset", function(e) {
      e.preventDefault();
      $('#overlay-context-search').val('');
    });

    $body.on("submit", ".js-context-form", function (event) {
      event.preventDefault();
      setTimeout(function () {
        $(selectors.articles).scrollTop(0);
        searchArticles(true);
      }, 200);
    });

    $body.on("keyup", selectors.search, function () {
      var $el = $(this),
        context = $el.val();

      searchInputAutoSize();

      if (context.length >= 2) {
        clearTimeout(searchDelay);

        searchDelay = setTimeout(function () {
          variables.searchQuery = context;
          $(selectors.articles).css("overflow-y", "hidden");
          $(".js-content").addClass("hidden");
          $(selectors.overlay).removeClass("opened").slideUp();
          setTimeout(searchArticles(true), 200);
          $(selectors.searchWrap).show().addClass("visible");
          $(selectors.search).val(variables.searchQuery);
        }, 500);
      }
    });

    $body.on("click", ".digest-link", function (ev) {
      ev.preventDefault();
      history.pushState(null, null, $(ev.currentTarget).attr("href"));
      window.dispatchEvent(new Event("locationchange"));

      resetAndShowArticles();
    });

    $(selectors.articles).on(
      "click",
      ".main-article a:not(.lightbox)",
      function (ev) {
        if (
          $(this).attr("href").indexOf("blog") != -1 &&
          $(this).attr("target") != "_blank"
        ) {
          ev.preventDefault();

          var $el = $(this),
            href = $el.attr("href"),
            parsedPath = parseURL(href).pathname,
            postSlug = parsedPath.replace("/blog/", "");

          $(".js-content").addClass("hidden");

          setTimeout(function () {
            resetState();
            getArticle(postSlug);
            history.pushState(null, null, "/" + postSlug);
            window.dispatchEvent(new Event("locationchange"));
          }, 200);
        }
      }
    );

    $(selectors.articles).on("scroll", function (e) {
      if (location.pathname.indexOf("blog") == -1) {
        if (!isDevEnv) eqHeight();
        var lastArticleOffset = $(selectors.articles)
          .find(".main-article:last")
          .offset().top;
        if (lastArticleOffset <= $(window).height()) {
          initSameHeight();
          if (variables.articlesScroll) {
            variables.articlesScroll = false;
            if (location.pathname.indexOf("search") !== -1) {
              if (
                present(variables.searchPage) &&
                present(variables.nextSearchPage)
              ) {
                variables.searchPage = variables.nextSearchPage;
                searchArticles();
              }
            } else if (present(variables.nextArticles)) {
              getArticles(variables.nextArticles);
            }
          }
        } else {
          variables.articlesScroll = true;
        }
      }
    });

    function changeArticle(article_slug, prev, next) {
      if (variables.articleScroll) {
        variables.articleScroll = false;

        if (present(prev)) variables.currentArticle--;
        else if (present(next)) variables.currentArticle++;

        setTimeout(function () {
          getArticle(article_slug, true, prev, next);
        }, 0);
      }
    }

    let currentArticle = 0;
    let locationPath = location.pathname.replace("/blog/", "");

    document
      .querySelector(selectors.article)
      .addEventListener("scroll", function (e) {
        var $articleBlock = $(selectors.article).find(".js-infinity-scroll"),
          showNextPosition;

        var articleBlockHeight = $articleBlock.outerHeight(),
          articleBlockOffset = $articleBlock.offset().top;

        if (isMobile.any) {
          showNextPosition = parseInt(
            articleBlockHeight +
              articleBlockOffset -
              $(window).height() -
              $(selectors.article).offset().top
          );
        } else {
          showNextPosition = parseInt(
            articleBlockHeight + articleBlockOffset - $(window).height()
          );
        }
        if (
          present(variables.relatedArticles) &&
          variables.currentArticle < variables.relatedArticles.length &&
          showNextPosition <= 500
        ) {
          changeArticle(
            variables.relatedArticles[variables.currentArticle].url,
            false,
            true
          );
        } else {
          variables.articleScroll = true;
        }

        for (let i in scrollPositionArticles) {
          let data = scrollPositionArticles[i];
          let postOffsetTop = data._elem.offset().top;
          let postHeight = data._elem.height();
          if (
            postOffsetTop <= 0 &&
            postOffsetTop + postHeight > 0 &&
            currentArticle != i
          ) {
            currentArticle = i;
            locationPath = data.slug;
            // Set title, meta and history
            document.title = data.title;
            $("head")
              .find(selectors.metas)
              .remove()
              .end()
              .append(data.metaTitle);
            history.replaceState(null, null, "/" + data.slug);
            window.dispatchEvent(new Event("locationchange"));
            break;
          }
        }
      });

    window.addEventListener("popstate", function (event) {
      initialContent(false);
    });
  });
  $(document).on("click", ".header__about", function (e) {
    resetSearchContext();
    $(".js-search").hide().removeClass("visible");
  });
  window.addEventListener("resize", eqHeight);
  // Расчёт высоты синего блока , относительно первой картинке в тройных блоках
  function eqHeight() {
    requestAnimationFrame(compute);
  }

  function compute() {
    var nodesImg = document.querySelectorAll(
      ".main-article__img-box picture img"
    );
    var textNodes = document.querySelectorAll(".main-article__text-box");

    var arr = [];

    nodesImg.forEach(function (img) {
      arr.push(parseFloat(getComputedStyle(img).height));
      img.style.maxHeight = 477 + "px";
    });

    var maxHeight = Math.max.apply(Math, arr);
    textNodes.forEach(function (textNode) {
      textNode.style.height = maxHeight + "px";
      return false;
    });
  }
  return {
    variables: variables,
  };
})();
var logoHeader = document.querySelector(".icn-plusone");

logoHeader.addEventListener("click", function (e) {
  e.preventDefault();
  let blockTags = document.querySelector(".materials-on-tag__wrap");
  blockTags && (blockTags.style.display = "none");
});

function changeCanonical() {
  const link = document.getElementById("canonicalLink");
  link &&
    link.setAttribute(
      "href",
      window.location.origin + window.location.pathname
    );
}

function cleanParams(arrParams) {
  for (let i = 0; i < arrParams.length; i++) {
    if (window.location.search.includes(arrParams[i])) {
      window.location = `${window.location.origin}${window.location.pathname}`;
    }
  }
}
document.addEventListener("DOMContentLoaded", function () {
  changeCanonical();
  // cleanParams(["?fbclid=", "?utm_"]);
});
window.addEventListener("locationchange", changeCanonical);
