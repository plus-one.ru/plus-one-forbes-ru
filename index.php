<?php
require_once('bootstrap.php');
?>
<!doctype html>
<html lang="ru">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WW68TLZ');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title data-title="Forbes +1">Forbes +1</title>
    <meta name="title" content="<?php echo $meta['title']?>"/>
    <meta name="description" content="<?php echo $meta['description']?>"/>
    <meta name="keywords" content="<?php echo $meta['keywords']?>"/>
    <meta name="fragment" content="!">
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicons/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicons/favicon-96x96.png">
    <link rel="manifest" href="/favicon-manifest.json">
    <link rel="canonical" href="<?php echo $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['HTTP_HOST']. $_SERVER['REDIRECT_URL']?>" id="canonicalLink">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/design/plusone/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="yandex-verification" content="af21ef7fbc76fc17" />

    <?php

    if ($ogMeta == true) {
        echo $result;
    } else {
        ?>
        <meta property="og:title" content="Forbes +1">
        <meta property="og:type" content="website">
        <meta property="og:image" content="<?php echo 'http://' . $rootUrl; ?>/assets/images/og_image.png">
        <meta property="og:url" content="<?php echo 'http://' . $rootUrl; ?>">
        <meta property="og:description" content="">
        <meta property="og:locale" content="ru_ru">
        <meta property="og:site_name" content="Forbes +1">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="Forbes +1">
        <meta name="twitter:description" content="">
        <meta name="twitter:url" content="<?php echo 'http://' . $rootUrl; ?>">
        <meta name="twitter:image" content="<?php echo 'http://' . $rootUrl; ?>/assets/images/og_image.png">
        <meta name="twitter:image:alt" content="Forbes +1">
        <meta name="twitter:site" content="Forbes +1">
        <?php
    }
    ?>

    <link rel="stylesheet" href="/css/main.css?v=6">
    <link rel="stylesheet" href="/css/blocked-content.css">

    <script src="//yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WW68TLZ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="wrapper js-content plusone-content">
    <div class="plusone-context-search js-search">
        <form action="" class="plusone-container plusone-form js-context-form">
            <input type="text" placeholder="Поиск" class="plusone-form-input" id="overlay-context-search" />
            <input type="button" class="plusone-form-reset js-context-form-reset" value="&times;">
        </form>
    </div>
    <header class="header header--collapsed">
        <div class="header__content inner-main">
            <a href="//www.forbes.ru/" target="_blank">На главную</a>
            <a href="/" class="header__actions icn-plusone digest-link"></a>
            <span class="header__about">О проекте</span>
        </div>
    </header>
    <header class="header header--extended js-overlay">
        <div class="header__content--large inner-main">
            <div class="header__topline">
                <span class="header__about">О проекте</span>
                <span class="header__close icn-c"></span>
            </div>
            <div class="header__box">
                <div class="header__text">
                    <p>+1&nbsp;(Плюс Один)&nbsp;&mdash; коммуникационный проект, рассказывающий о&nbsp;лидерских практиках в&nbsp;области социальной и&nbsp;экологической ответственности. Мы&nbsp;создаем площадку для прямой коммуникации и&nbsp;обмена ресурсами между бизнесом, некоммерческими организациями, государством и&nbsp;обществом.</p>
                    <br/><p>Миссия +1&nbsp;&mdash; содействовать развитию инфраструктуры рынка устойчивого развития. Наши партнеры создают социально значимые проекты, мы&nbsp;рассказываем о&nbsp;них массовой аудитории, тем самым формируя культуру ответственного производства, потребления и&nbsp;инвестирования.</p>
                    <br/><p>Мы&nbsp;активно сотрудничаем с&nbsp;крупнейшими российскими медиа и&nbsp;развиваем совместные площадки:</p>
                    <br/><p><a href="//plus-one.ru" target="_blank" rel="noopener noreferrer" title="+1">+1</a></p>
                    <br/><p><a href="//plus-one.rbc.ru" target="_blank" rel="noopener noreferrer" title="РБК+1">РБК+1</a></p>
                    <br/><p><a href="//plus-one.vedomosti.ru" target="_blank" rel="noopener noreferrer" title="Ведомости+1">Ведомости+1</a></p>
                </div>
                <div class="header__info">
                        <p>Редакция +1&nbsp;&mdash; Юрий Яроцкий<br /><a href="mailto:y.yarotsky@plus-one.ru" title="Юрий Яроцкий">y.yarotsky@plus-one.ru</a></p><br/>
                        <p>Шеф-редактор Forbes+1&nbsp;&mdash; Маргарита Федорова<br /><a href="mailto:m.fedorova@plus-one.ru" title="Маргарита Федорова">m.fedorova@plus-one.ru</a></p><br/>
                        <p>Мероприятия +1&nbsp;&mdash; Вероника Васильева<br /><a href="mailto:v.vasilyeva@plus-one.ru" title="Вероника Васильева">v.vasilyeva@plus-one.ru</a></p>
                </div>
                <div class="header__social">
                    <!-- <a href="//www.facebook.com/ProjectPlus1Official" target="_blank" class="header__social-item icn-fb"></a> -->
                    <a href="https://vk.com/project_plus_one" target="_blank" class="header__social-item icn-vk"></a>
                    <a href="https://twitter.com/project_plusone" target="_blank" class="header__social-item icn-tw"></a>
                </div>
            </div>
            <div class="plusone-overlay__footer">
                <div class="plusone-overlay__footer-item search">
                    <input type="text" placeholder="Поиск" class="plusone-form-input" id="overlay-bottom-search" />
                </div>
                <div class="plusone-overlay__footer-item plusone-logo">
                    <a href="/" class="icn-plusone digest-link js-overlay-trigger"></a>
                </div>
                <div class="plusone-overlay__footer-item forbes-logo">
                    <img src="/images/logo.svg" alt="Forbes" />
                </div>
            </div>
        </div>
    </header>

    <div id="articles-list" class="container plusone-articles-list">
        <?php echo $blogClass->body; ?>
    </div>
    <div id="article-single" class="plusone-article-single">
        <div class="js-infinity-scroll"></div>
    </div>
</div>
</body>
<script type="text/javascript" src="/assets/javascripts/libs/jquery.min.js"></script>
<script type="text/javascript" src="/assets/javascripts/libs/request-animation-frame.js"></script>
<script type="text/javascript" src="/assets/javascripts/libs/is-mobile.min.js"></script>
<script type="text/javascript" src="/assets/javascripts/libs/retina-cover.min.js"></script>
<script type="text/javascript" src="/assets/javascripts/application.js?v=20"></script>
<script type="text/javascript" src="/js/jquery.main.js"></script>
<script type="text/javascript" src="/bundle/main.bdb9986ee97a1987cefa.js"></script>
</body>
</html>
